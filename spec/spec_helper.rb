$LOAD_PATH.unshift File.expand_path("../../lib", __FILE__)
require "eventscraper"

require 'webmock/rspec'

RSpec.configure do |config|
  config.before(:each) do
    stub_request(:get, 'http://www.wegottickets.com/searchresults/all').
      to_return(
        body: File.open('spec/stubs/all.html'),
        headers: {'Content-Type' => 'text/html; charset=ISO-8859-1', 'Accept'=>'*/*', 'Accept-Encoding'=>'gzip, deflate', 'Host'=>'www.wegottickets.com', 'User-Agent'=>'rest-client/2.0.0 (darwin16.0.0 x86_64) ruby/2.3.1p112'},
        status: 200
      )
  end
end

WebMock.disable_net_connect!(allow_localhost: true)
