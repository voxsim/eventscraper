require "spec_helper"

describe Eventscraper do
  it "has a version number" do
    expect(Eventscraper::VERSION).not_to be nil
  end

  it "retrieve events and save a json file" do
    expect(Eventscraper.run).to eq('[{"artist":"ADAM HOLMES AND THE EMBERS","city":"BIDDULPH","venue":" Old Sam\'s","date":"Wed 18th Jan, 2017, 7:30pm","price":"£13.20"},{"artist":"ALAN BARNES & GILAD ATZMON - \'THE LOWEST COMMON DENOMINATOR\'","city":"LONDON ","venue":" Spice of Life","date":"Wed 18th Jan, 2017, 7:00pm","price":"£8.80"},{"artist":"ALLSOPP / STOREY / IRELAND","city":"LONDON","venue":" Vortex","date":"Wed 18th Jan, 2017, 8:00pm","price":"£11.00"},{"artist":"AMBER ARCADES","city":"LONDON","venue":" OSLO","date":"Wed 18th Jan, 2017, 7:30pm","price":"£9.35"},{"artist":"ARRIVAL (12A)","city":"","venue":"Ely Cinema The Maltings, Ship Lane, CB7 4BB","date":"18th Jan - doors 7pm for 7.30pm film","price":""},{"artist":"BALANCE AND COMPOSURE","city":"BRIGHTON","venue":" Sticky Mike\'s Frog Bar","date":"Wed 18th Jan, 2017, 7:00pm","price":"£15.40"},{"artist":"DARIA KULESH AT EPPING FOLK CLUB","city":"EPPING","venue":" St. John the Baptist Church","date":"Wed 18th Jan, 2017, 7:00pm","price":"£13.75"},{"artist":"EASTBOURNE SCEPTICS: PLACEBO: MIND OVER MATTER?","city":"EASTBOURNE","venue":" The Printers Playhouse","date":"Wed 18th Jan, 2017, 8:00pm","price":"£3.30"},{"artist":"EDWARDALICE + SUPPORT","city":"HOVE","venue":" The Brunswick","date":"Wed 18th Jan, 2017, 8:00pm","price":"£3.85"},{"artist":"GARY FLETCHER & TOM LEARY","city":"LOWDHAM","venue":" The Old Ship Inn","date":"Wed 18th Jan, 2017, 7:30pm","price":"£11.00"}]')
  end
end
