require "spec_helper"

describe Eventscraper::Parser::Page do
  it "parses the page for events" do
    html = instance_double('Document')

    expect(html).to receive(:css).with('div.content.block-group.chatterbox-margin').and_return([])

    parser = Eventscraper::Parser::Page.new([])
    parser.execute(html)
  end

  it "generates an array of event which everyone hold information from other parsers" do
    html = instance_double('Document')
    html_event = instance_double('HtmlEvent')
    parser = instance_double('Parser')

    expect(html).to receive(:css).with('div.content.block-group.chatterbox-margin').and_return([html_event])
    expect(parser).to receive(:name)
    expect(parser).to receive(:execute)

    parser = Eventscraper::Parser::Page.new([parser])
    parser.execute(html)
  end
end
