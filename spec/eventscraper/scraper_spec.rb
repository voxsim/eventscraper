require "spec_helper"

describe Eventscraper::Scraper do
  it "download the page from the url" do
    expect(RestClient).to receive(:get).with('myurl')

    scraper = Eventscraper::Scraper.new
    scraper.download('myurl')
  end
end
