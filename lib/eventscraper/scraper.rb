require 'nokogiri'
require 'rest-client'

module Eventscraper
  class Scraper
    def download(url)
      Nokogiri::HTML(RestClient.get(url))
    end
  end
end
