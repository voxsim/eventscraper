module Eventscraper
  module Parser
    class Artist
      def name
        'artist'
      end

      def execute(html)
        html.css('.event_link').text
      end
    end
  end
end
