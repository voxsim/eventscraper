module Eventscraper
  module Parser
    class City
      def name
        'city'
      end

      def execute(html)
        city(html)
      end

      private

      def city(html)
        venueDetails = html.css('.venue-details h4')[0].text

        if venueDetails.include? ':'
          venueDetails.split(':')[0]
        else
          ''
        end
      end
    end
  end
end
