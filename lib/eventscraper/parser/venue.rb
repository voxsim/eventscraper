module Eventscraper
  module Parser
    class Venue
      def name
        'venue'
      end

      def execute(html)
        venue(html)
      end

      private

      def venue(html)
        venueDetails = html.css('.venue-details h4')[0].text

        if venueDetails.include? ':'
          venueDetails.split(':')[1]
        else
          venueDetails
        end
      end
    end
  end
end
