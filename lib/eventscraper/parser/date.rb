module Eventscraper
  module Parser
    class Date
      def name
        'date'
      end

      def execute(html)
        html.css('.venue-details h4')[1].text
      end
    end
  end
end
