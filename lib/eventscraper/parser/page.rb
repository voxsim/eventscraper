module Eventscraper
  module Parser
    class Page
      def initialize(parsers)
        @parsers = parsers
      end

      def name
        'page'
      end

      def execute(html)
        events = []
        html.css('div.content.block-group.chatterbox-margin').each do |html_event|
          events << build_event(html_event)
        end
        events
      end

      private

      def build_event(html_event)
        event = {}
        @parsers.each do |parser|
          event[parser.name] = parser.execute(html_event)
        end
        event
      end
    end
  end
end
