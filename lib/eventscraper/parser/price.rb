module Eventscraper
  module Parser
    class Price
      def name
        'price'
      end

      def execute(html)
        html.css('.searchResultsPrice strong').text
      end
    end
  end
end
