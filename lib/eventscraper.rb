require 'eventscraper/version'
require 'eventscraper/scraper'
require 'eventscraper/parser/artist'
require 'eventscraper/parser/city'
require 'eventscraper/parser/date'
require 'eventscraper/parser/page'
require 'eventscraper/parser/price'
require 'eventscraper/parser/venue'

require 'json'

module Eventscraper
  class << self
    def run
      url = 'http://www.wegottickets.com/searchresults/all'
      scraper = Scraper.new
      parser = Parser::Page.new(
        [
          Parser::Artist.new,
          Parser::City.new,
          Parser::Venue.new,
          Parser::Date.new,
          Parser::Price.new
        ]
      )

      page = scraper.download(url)
      report = parser.execute(page)
      JSON.generate(report)
    end
  end
end
