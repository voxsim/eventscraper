# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'eventscraper/version'

Gem::Specification.new do |spec|
  spec.name          = "eventscraper"
  spec.version       = Eventscraper::VERSION
  spec.authors       = ["Simon Vocella"]
  spec.email         = ["voxsim@gmail.com"]

  spec.summary       = %q{Scraping of events in http://www.wegottickets.com/searchresults/all}
  spec.description   = %q{Scraping of events in http://www.wegottickets.com/searchresults/all}
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency "nokogiri", "~> 1.7"
  spec.add_dependency "rest-client", "~> 2.0"
  spec.add_dependency "json", "~> 2.0"

  spec.add_development_dependency "bundler", "~> 1.13"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency "webmock", "~> 2.3"
end
